import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'City guide',
      home: Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.blue,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('2guides'),
                Container(
                  child: Image.network(
                      'https://alllogos.ru/images/logotip-city-guide.png',
                      color: Colors.white),
                  padding: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                ),
              ],
            )),
        body: Center(
          child: ListView(
            padding: EdgeInsets.all(9.0),
            children: <Widget>[
              Text('Гид города Мариуполь',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontStyle: FontStyle.italic, fontSize: 20.0)),
              Divider(
                height: 12.0,
                thickness: 3.0,
                color: Colors.lightBlueAccent,
              ),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Card(
                      elevation: 10.0,
                      color: Colors.blueAccent,
                      child: InkWell(
                        onTap: launchURL1,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(9.0, 9.0, 0.0, 0.0),
                          height: 70.0,
                          child: ListTile(
                            trailing: Text('Вперед!', style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),),
                            leading: Icon(Icons.account_balance, color: Colors.white,),
                            title: Text('Интересные места'),
                            subtitle: Text('Места, которые стоит посетить!'),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      elevation: 10.0,
                      color: Colors.blueAccent,
                      child: InkWell(
                        onTap: () {
                          launchURL2;
                        },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(9.0, 9.0, 0.0, 0.0),
                          height: 70.0,
                          child: ListTile(
                              trailing: Text('Вперед!',
                                  style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold)),
                              leading: Icon(Icons.account_balance, color: Colors.white,),
                              title: Text('Объявления'),
                              subtitle: Text('Самые дешевые цены города!')),
                        ),
                      ),
                    ),
                    Card(
                      elevation: 10.0,
                      color: Colors.blueAccent,
                      child: InkWell(
                        onTap: () {
                          launchURL3;
                          },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(9.0, 9.0, 0.0, 0.0),
                          height: 70.0,
                          child: ListTile(
                              trailing: Text('Вперед!',
                                  style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold)),
                              leading: Icon(Icons.account_balance, color: Colors.white,),
                              title: Text('Досуг'),
                              subtitle: Text('Кафе, рестораны, бары...')),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                height: 12.0,
                thickness: 3.0,
                color: Colors.lightBlueAccent,
              ),
//              Container(
//                child: ListView(
//                scrollDirection: Axis.vertical,
//                children: <Widget>[
//                  horizontalList1,
//                  horizontalList2,
//                ],
//              ),
//              ),
            ],
          ),
        ),
      ),
    );
  }



//  Widget horizontalList1 = new Container(
//      margin: EdgeInsets.symmetric(vertical: 20.0),
//      height: 200.0,
//      child: new ListView(
//        scrollDirection: Axis.horizontal,
//        children: <Widget>[
//          Container(width: 160.0, color: Colors.red,),
//          Container(width: 160.0, color: Colors.orange,),
//          Container(width: 160.0, color: Colors.pink,),
//          Container(width: 160.0, color: Colors.yellow,),
//        ],
//      )
//  );

//  Widget horizontalList2 = new Container(
//      margin: EdgeInsets.symmetric(vertical: 20.0),
//      height: 200.0,
//      child: new ListView(
//        scrollDirection: Axis.horizontal,
//        children: <Widget>[
//          Container(width: 160.0, color: Colors.blue,),
//          Container(width: 160.0, color: Colors.green,),
//          Container(width: 160.0, color: Colors.cyan,),
//          Container(width: 160.0, color: Colors.black,),
//        ],
//      )
//  );


launchURL1 () async {
  const url = 'https://www.tripadvisor.ru/Attractions-g298055-Activities-Mariupol_Donetsk_Oblast.html';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
  }
  launchURL2 () async {
    const url = 'https://www.0629.com.ua/ads';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  launchURL3 () async {
  const url = 'https://tomato.ua/Mariupol';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
  }
}
